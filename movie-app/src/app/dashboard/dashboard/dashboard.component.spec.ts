import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { DashboardComponent } from './dashboard.component';
import { IUser, AuthService } from 'src/app/auth/auth.service';
import { IMovieResult } from 'src/app/shared/model';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MovieKeyInterceptor } from 'src/app/shared/movie-key.interceptor';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MovieCollectionService } from 'src/app/shared/movie-collection.service';

describe('DashboardComponent', () => {
  let movieCollectionService: MovieCollectionService;
  let dashboardComponent: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const user = <IUser>{
    id: 1,
    firstName: 'Test',
    lastName: 'User'
  };

  const movieMatrix = <IMovieResult>{
    popularity: 30.988,
    vote_count: 15939,
    video: false,
    poster_path: '/gynBNzwyaHKtXqlEKKLioNkjKgN.jpg',
    id: 603,
    adult: false,
    backdrop_path: '/icmmSD4vTTDKOq2vvdulafOGw93.jpg',
    original_language: 'en',
    original_title: 'The Matrix',
    genre_ids: [28, 878],
    title: 'The Matrix',
    vote_average: 8.1,
    overview: 'Set in the 22nd century, The Matrix tells the story of a computer hacker who joins a group of underground insurgents fig...',
    release_date: '1999-03-30'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [BrowserAnimationsModule, RouterTestingModule, HttpClientModule, MatTableModule, MatPaginatorModule],
      providers: [
        DashboardComponent,
        AuthService,
        MovieCollectionService,
        { provide: HTTP_INTERCEPTORS, useClass: MovieKeyInterceptor, multi: true }],
      schemas: [NO_ERRORS_SCHEMA]
    }).overrideTemplate(DashboardComponent, '');
  }));

  beforeEach(() => {
    movieCollectionService = TestBed.get(MovieCollectionService);
    fixture = TestBed.createComponent(DashboardComponent);
    dashboardComponent = fixture.componentInstance;
    fixture.detectChanges();
    movieCollectionService.clearUserData(user);
  });

  it('should create', () => {
    expect(dashboardComponent).toBeTruthy();
  });

  it('should remove movie from user', async (done) => {
    movieCollectionService.saveMovieToProfile(user, movieMatrix).subscribe(savedMovies => { // Add movie
      done();
      expect(savedMovies.length).toBe(1);
      dashboardComponent.removeMovie(user, movieMatrix).subscribe(movies => { // Remove movie
        done();
        expect(movies.length).toBe(0);
      });
    });
  });
});
