import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MovieCollectionService } from 'src/app/shared/movie-collection.service';
import { AuthService, IUser } from 'src/app/auth/auth.service';
import { map, switchMap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { IMovieResult, IPersonDetails } from 'src/app/shared/model';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { UserService } from 'src/app/shared/user.service';
import { PersonCollectionService } from 'src/app/shared/person-collection.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  userInfo: IUser;
  movies: IMovieResult[];
  people: IPersonDetails[];
  displayedColumns: string[] = ['title', 'release_date', 'overview', 'navigate'];
  peopleDisplayedColumns: string[] = ['name', 'known_for_department', 'birthday', 'navigate'];
  dataSource: MatTableDataSource<IMovieResult>;
  peopleDataSource: MatTableDataSource<IPersonDetails>;
  hasResults = true;
  hasPeopleResults = true;

  @ViewChild('datatable') datatable: MatTable<IMovieResult[]>;
  @ViewChild('peopledatatable') peopledatatable: MatTable<IMovieResult[]>;
  @ViewChild('moviePaginator', { read: MatPaginator }) paginator: MatPaginator;
  @ViewChild('peoplePaginator', { read: MatPaginator }) peoplePaginator: MatPaginator;
  @ViewChild('movieTable', { read: MatSort }) movieSort: MatSort;
  @ViewChild('personTable', { read: MatSort }) personSort: MatSort;

  constructor(
    private movieCollectionService: MovieCollectionService,
    private personCollectionService: PersonCollectionService,
    private authService: AuthService,
    private userService: UserService
  ) { }

  // #Map
  ngOnInit() {
    this.subscriptions.push(
      this.authService.getUserInfo().pipe(
        switchMap(userInfo =>
          this.userService.getUserData(userInfo.id).pipe(
            map(userData => [userInfo, userData])
          )
        )
      ).subscribe(([userInfo, userData]) => {
        this.userInfo = userInfo as IUser;
        if (userData) {
          this.movies = userData['movies'] as IMovieResult[];
          this.people = userData['people'] as IPersonDetails[];

          if (this.movies) {
            this.dataSource = new MatTableDataSource(this.movies);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.movieSort;

            if (this.movies.length === 0) {
              this.hasResults = false;
            } else {
              this.hasResults = true;
            }
          }

          if (this.people) {
            this.peopleDataSource = new MatTableDataSource(this.people);
            this.peopleDataSource.paginator = this.peoplePaginator;
            this.peopleDataSource.sort = this.personSort;

            if (this.people.length === 0) {
              this.hasPeopleResults = false;
            } else {
              this.hasPeopleResults = true;
            }
          }
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  onClickRemoveMovie(movie: IMovieResult) {
    this.subscriptions.push(
      this.removeMovie(this.userInfo, movie).subscribe()
    );
  }

  onClickRemovePerson(person: IPersonDetails) {
    this.subscriptions.push(
      this.removePerson(this.userInfo, person).subscribe()
    );
  }
  // #Arrow functions
  removeMovie(userInfo: IUser, movie: IMovieResult): Observable<IMovieResult[]> {
    return this.movieCollectionService.removeMovieFromProfile(userInfo, movie).pipe(
      map(movies => {
        this.movies = movies;
        this.dataSource = new MatTableDataSource(movies);
        return movies;
      })
    );
  }

  // #Arrow functions
  removePerson(userInfo: IUser, person: IPersonDetails): Observable<IPersonDetails[]> {
    return this.personCollectionService.removePersonFromProfile(userInfo, person).pipe(
      map(people => {
        this.people = people;
        this.peopleDataSource = new MatTableDataSource(people);
        return people;
      })
    );
  }
}
