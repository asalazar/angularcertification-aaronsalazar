import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PersonDetailsComponent } from './person-details.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  { path: 'details/person/:personId', component: PersonDetailsComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  exports: [
    RouterModule
  ]
})
export class PersonDetailsModule { }
