import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { IPersonDetails, IMovieCredits } from '../shared/model';

@Injectable({
  providedIn: 'root'
})
export class PersonDetailsService {
  readonly personUrl = 'https://api.themoviedb.org/3/person/';

  constructor(
    private http: HttpClient
  ) { }

  getPerson(id: number): Observable<IPersonDetails> {
    return this.http.get<any>(`${this.personUrl}${id}?`)
      .pipe(catchError(this.handleError));
  }

  getMovieCredits(id: number): Observable<IMovieCredits> {
    return this.http.get<any>(`${this.personUrl}${id}/movie_credits?`)
    .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return throwError(err.message);
  }
}
