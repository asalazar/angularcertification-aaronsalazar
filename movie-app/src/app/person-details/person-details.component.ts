import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PersonDetailsService } from './person-details.service';
import { AuthService, IUser } from '../auth/auth.service';
import { PersonService } from '../shared/person.service';
import { MovieService } from '../shared/movie.service';
import { PersonCollectionService } from '../shared/person-collection.service';
import { IPersonDetails, ICast, ICrew, IMovieCredits } from '../shared/model';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css']
})
export class PersonDetailsComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  personId: number;
  person: IPersonDetails;
  isLoggedIn: boolean;
  user: IUser;
  truncatedBiography: string;
  movieCast: ICast[];
  movieCrew: ICrew[];
  userHasPerson = false;

  constructor(
    private route: ActivatedRoute,
    private personDetailsService: PersonDetailsService,
    private authService: AuthService,
    private personService: PersonService,
    private movieService: MovieService,
    private personCollectionService: PersonCollectionService
  ) { }

  ngOnInit() {
    this.personId = +this.route.snapshot.paramMap.get('personId');

    this.personDetailsService.getPerson(this.personId).subscribe((person: IPersonDetails) => {
      this.person = person;
      this.truncatedBiography = person.biography.substring(0, 50).concat('...');

      this.subscriptions.push(
        this.authService.isLoggedIn.subscribe(result => {
          this.isLoggedIn = result;
          if (this.isLoggedIn) {
            this.subscriptions.push(
              this.authService.getUserInfo().subscribe(user => {
                this.user = user;
                this.userHasPerson = this.personCollectionService.userHasPerson(user, this.person);
              })
            );
          }
        })
      );
    });

    this.subscriptions.push(
      this.personDetailsService.getMovieCredits(this.personId).subscribe((credits: IMovieCredits) => {
        this.movieCast = credits.cast.slice(0, 5);
        this.movieCrew = credits.crew.slice(0, 5);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscriptions => {
      subscriptions.unsubscribe();
    });
  }

  getProfilePath(path: string): string {
    return this.personService.getProfilePath(path);
  }

  getPosterPath(path: string): string {
    return this.movieService.getPosterPath(path, 'small');
  }

  updatePerson(isChecked: boolean) {
    if (isChecked) {
      this.savePerson();
    } else {
      this.removePerson();
    }
  }

  savePerson() {
    this.subscriptions.push(
      this.personCollectionService.savePersonToProfile(this.user, this.person).subscribe()
    );
    this.userHasPerson = true;
  }

  removePerson() {
    this.subscriptions.push(
      this.personCollectionService.removePersonFromProfile(this.user, this.person).subscribe()
    );
    this.userHasPerson = false;
  }
}
