import { TestBed, async } from '@angular/core/testing';
import { MovieCollectionService } from './movie-collection.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MovieKeyInterceptor } from './movie-key.interceptor';
import { IMovieResult } from './model';
import { IUser } from '../auth/auth.service';

describe('MovieCollectionService', () => {
  let movieCollectionService: MovieCollectionService;
  const user = <IUser>{
    id: 1,
    firstName: 'Test',
    lastName: 'User'
  };

  const movieMatrix = <IMovieResult>{
    popularity: 30.988,
    vote_count: 15939,
    video: false,
    poster_path: '/gynBNzwyaHKtXqlEKKLioNkjKgN.jpg',
    id: 603,
    adult: false,
    backdrop_path: '/icmmSD4vTTDKOq2vvdulafOGw93.jpg',
    original_language: 'en',
    original_title: 'The Matrix',
    genre_ids: [28, 878],
    title: 'The Matrix',
    vote_average: 8.1,
    overview: 'Set in the 22nd century, The Matrix tells the story of a computer hacker who joins a group of underground insurgents fig...',
    release_date: '1999-03-30'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [MovieCollectionService, { provide: HTTP_INTERCEPTORS, useClass: MovieKeyInterceptor, multi: true }]
    });

    movieCollectionService = TestBed.get(MovieCollectionService);

    movieCollectionService.clearUserData(user).subscribe();
  }));

  it('should get array of IMovie from user', async (done) => {
    movieCollectionService.saveMovieToProfile(user, movieMatrix).subscribe(() => {
      movieCollectionService.getUsersMovies(user.id).subscribe(movies => {
        done();
        expect(movies.length).toBeGreaterThan(0);
      });
    });
  });

  it('should save movie', async (done) => {
    movieCollectionService.saveMovieToProfile(user, movieMatrix).subscribe(movies => {
      done();
      expect(movies.length).toBeGreaterThan(0);
    });
  });

  it('should remove movie', async (done) => {
    movieCollectionService.saveMovieToProfile(user, movieMatrix).subscribe(() => {
      movieCollectionService.removeMovieFromProfile(user, movieMatrix).subscribe(movies => {
        done();
        expect(movies.length).toBe(0);
      });
    });
  });
});
