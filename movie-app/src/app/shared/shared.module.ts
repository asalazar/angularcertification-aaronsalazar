import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MovieKeyInterceptor } from './movie-key.interceptor';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ResultsActionsComponent } from './results-actions/results-actions.component';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [ResultsActionsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSortModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatCardModule,
    MatSlideToggleModule,
    ResultsActionsComponent,
    MatSortModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: MovieKeyInterceptor, multi: true }
  ]
})
export class SharedModule { }
