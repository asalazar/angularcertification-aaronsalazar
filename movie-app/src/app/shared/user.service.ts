import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IUserData } from './model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getUserData(userId: number): Observable<IUserData> {
    const storage = localStorage.getItem(userId.toString());
    return of(storage ? JSON.parse(storage) : null);
  }
}
