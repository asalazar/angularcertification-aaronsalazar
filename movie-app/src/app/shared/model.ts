// Movie definitions
export interface IMovie {
  page: number;
  results: IMovieResult[];
  total_pages: number;
  total_results: number;
}

export interface IMovieResult {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean
  vote_average: number;
  vote_count: number;
}

// Person definitions
export interface IPerson {
  page: number;
  total_results: number;
  total_pages: number;
  results: IPersonResult[];
}

export interface IPersonResult {
  popularity: number;
  known_for_department: string;
  gender: number;
  id: number;
  profile_path: string;
  adult: boolean;
  known_for: IMovieResult[];
  name: string;
}

export interface IPersonDetails {
  birthday: string;
  known_for_department: string;
  deathday: string;
  id: number;
  name: string;
  also_known_as: string[];
  gender: number;
  biography: string;
  popularity: number;
  place_of_birth: string;
  profile_path: string;
  adult: boolean;
  imdb_id: string;
  homepage: string;
}

export interface IMovieCredits {
  cast: ICast[];
  crew: ICrew[];
}

export interface ICast {
  character: string;
  credit_id: string;
  release_date: string;
  vote_count: number;
  video: boolean;
  adult: boolean;
  vote_average: number;
  title: string;
  genre_ids: number[];
  original_language: string;
  original_title: string;
  popularity: number;
  id: number;
  backdrop_path: string;
  overview: string;
  poster_path: string;
}

export interface ICrew {
  id: number;
  department: string;
  original_language: string;
  original_title: string;
  job: string;
  overview: string;
  vote_count: number;
  video: boolean;
  release_date: string;
  vote_average: number;
  title: string;
  popularity: number;
  genre_ids: number[];
  backdrop_path: string;
  adult: boolean;
  poster_path: string;
  credit_id: string;
}

// User definitions
export interface IUserData {
  people: IPersonDetails[];
  movies: IMovieResult[];
}