import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IUser } from 'src/app/auth/auth.service';
import { IMovieResult, IUserData } from './model';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class MovieCollectionService {

  constructor(
    private userService: UserService
  ) { }

  getUsersMovies(userId: number): Observable<IMovieResult[]> {
    return this.userService.getUserData(userId).pipe(
      map(data => {
        return data.movies;
      })
    );
  }

  saveMovieToProfile(user: IUser, movie: IMovieResult): Observable<IMovieResult[]> {
    return this.userService.getUserData(user.id).pipe(
      map(storage => {
        storage = storage || <IUserData>{ movies: [], people: [] };
        if (!Object.prototype.hasOwnProperty.call(storage, 'userMovies')) {
          storage['userMovies'] = [];
        }
        if (!storage.movies.find(m => m.id === movie.id)) {
          storage.movies.push(movie);
          localStorage.setItem(user.id.toString(), JSON.stringify(storage));
        }
        return storage.movies;
      })
    );
  }

  removeMovieFromProfile(user: IUser, movie: IMovieResult): Observable<IMovieResult[]> {
    return this.userService.getUserData(user.id).pipe(
      map(storage => {
        const movieIndexToRemove = storage.movies.findIndex(m => m.id === movie.id);
        if (movieIndexToRemove >= 0) {
          storage.movies.splice(movieIndexToRemove, 1);
          localStorage.setItem(user.id.toString(), JSON.stringify(storage));
        }
        return storage.movies;
      })
    );
  }

  userHasMovie(user: IUser, movie: IMovieResult): boolean {
    const data = JSON.parse(localStorage.getItem(user.id.toString()));
    return data ? !!data.movies.find(m => m.id === movie.id) : false;
  }

  clearUserData(user: IUser): Observable<void> {
    return of(localStorage.clear());
  }
}
