import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMovieResult } from '../model';

@Component({
  selector: 'app-results-actions',
  templateUrl: './results-actions.component.html',
  styleUrls: ['./results-actions.component.css']
})
export class ResultsActionsComponent implements OnInit {
  // #Inputs
  @Input() show: boolean;
  @Input() isInCollection: boolean;
  @Output() buttonClicked = new EventEmitter<IMovieResult>();

  constructor() { }

  ngOnInit() { }

  clicked() {
    this.buttonClicked.emit();
  }
}