import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
// #Services
export class MovieService {
  private readonly backdropPath = 'https://image.tmdb.org/t/p/w300';
  private readonly posterPath = 'https://image.tmdb.org/t/p/w154';
  private readonly smallPoserPath = 'https://image.tmdb.org/t/p/w92';
  private readonly productionCompanyLogoPath = 'https://image.tmdb.org/t/p/w92';

  constructor() { }

  getBackdropPath(path: string): string {
    return `${this.backdropPath}${path}`;
  }

  /**
   * Returns a valid path to a movie poster.
   * @param path
   * @param size small or large
   */
  getPosterPath(path: string, size?: string): string {
    if (size === 'small') {
      return `${this.smallPoserPath}${path}`;
    } else if (size === 'large') {
      return `${this.posterPath}${path}`;
    } else {
      return `${this.posterPath}${path}`;
    }
  }

  getProductionCompanyLogoPath(path: string): string {
    return `${this.productionCompanyLogoPath}${path}`;
  }
}
