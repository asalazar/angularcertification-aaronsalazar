import { TestBed } from '@angular/core/testing';

import { PersonCollectionService } from './person-collection.service';

describe('PersonCollectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersonCollectionService = TestBed.get(PersonCollectionService);
    expect(service).toBeTruthy();
  });
});
