import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IUser } from '../auth/auth.service';
import { map } from 'rxjs/operators';
import { UserService } from './user.service';
import { IUserData, IPersonDetails } from './model';

@Injectable({
  providedIn: 'root'
})
export class PersonCollectionService {

  constructor(
    private userService: UserService
  ) { }

  savePersonToProfile(user: IUser, person: IPersonDetails): Observable<IPersonDetails[]> {
    return this.userService.getUserData(user.id).pipe(
      map(storage => {
        storage = storage || <IUserData>{ movies: [], people: [] };
        if (!Object.prototype.hasOwnProperty.call(storage, 'people')) {
          storage['people'] = [];
        }
        if (!storage.people.find(p => p.id === person.id)) {
          storage.people.push(person);
          localStorage.setItem(user.id.toString(), JSON.stringify(storage));
        }
        return storage.people;
      })
    );
  }

  removePersonFromProfile(user: IUser, person: IPersonDetails): Observable<IPersonDetails[]> {
    return this.userService.getUserData(user.id).pipe(
      map(storage => {
        const personIndexToRemove = storage.people.findIndex(m => m.id === person.id);
        if (personIndexToRemove >= 0) {
          storage.people.splice(personIndexToRemove, 1);
          localStorage.setItem(user.id.toString(), JSON.stringify(storage));
        }
        return storage.people;
      })
    );
  }

  userHasPerson(user: IUser, person: IPersonDetails): boolean {
    const data = JSON.parse(localStorage.getItem(user.id.toString()));
    return data ? !!data.people.find(m => m.id === person.id) : false;
  }
}
