import { TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MovieKeyInterceptor } from '../shared/movie-key.interceptor';
import { SearchService } from '../search/search.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { IMovie } from './model';

describe('MovieKeyInterceptor', () => {
  let searchService: SearchService;
  let httpMock: HttpTestingController;
  const query = 'matrix';
  const year = 1999;
  const key = '6d1e4571239ebe5c68cf21a051f53e02';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        SearchService,
        { provide: HTTP_INTERCEPTORS, useClass: MovieKeyInterceptor, multi: true }
      ]
    });

    searchService = TestBed.get(SearchService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should add key to end of query.', () => {
    searchService.searchMovies('matrix', 1999).subscribe((movies: IMovie) => {
      expect(movies).toBeTruthy();
    });

    httpMock.expectOne(`${searchService.searchUrl}query=${query}&page=1&include_adult=false&year=${year}&api_key=${key}&language=en-US`);
  });
});
