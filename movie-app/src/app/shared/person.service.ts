import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private readonly profilePath = 'https://image.tmdb.org/t/p/w154';

  constructor() { }

  getProfilePath(path: string): string {
    return `${this.profilePath}${path}`;
  }
}
