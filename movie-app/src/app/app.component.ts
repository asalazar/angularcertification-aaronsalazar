import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  title = 'app';
  isLoggedIn = false;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.subscriptions.push(
      this.authService.isLoggedIn.subscribe(isLoggedIn => {
        this.isLoggedIn = isLoggedIn;
      })
    )
  }

  ngOnDestroy() {
    /**
     * This is how to unsubscribe from subscriptions
     */
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  logout() {
    this.authService.logout();
  }
}
