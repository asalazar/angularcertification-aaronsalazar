import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { IMovieResult } from '../shared/model';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsService {
  readonly movieUrl = 'https://api.themoviedb.org/3/movie/';

  constructor(
    private http: HttpClient
  ) { }

  // #Observables
  getMovie(id: number): Observable<IMovieResult> {
    return this.http.get<any>(`${this.movieUrl}${id}?`)
      .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return throwError(err.message);
  }
}
