import { TestBed, inject } from '@angular/core/testing';
import { MovieDetailsService } from './movie-details.service';
import { HttpModule, XHRBackend } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('MovieDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        MovieDetailsService,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    });
  });

  it('should be created (it)', inject([MovieDetailsService, XHRBackend, HttpClientModule], (_movieDetailsService: MovieDetailsService) => {
    const service: MovieDetailsService = TestBed.get(MovieDetailsService);
    expect(service).toBeTruthy();
  }));
});
