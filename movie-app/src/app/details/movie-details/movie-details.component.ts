import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieDetailsService } from '../movie-details.service';
import { IMovieResult } from 'src/app/shared/model';
import { MovieService } from 'src/app/shared/movie.service';
import { AuthService, IUser } from 'src/app/auth/auth.service';
import { MovieCollectionService } from 'src/app/shared/movie-collection.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  movieId: number;
  movie: IMovieResult;
  truncatedOverview: string;
  isLoggedIn: boolean;
  user: IUser;
  userHasMovie = false;

  constructor(
    private route: ActivatedRoute,
    private movieDetailsService: MovieDetailsService,
    private movieService: MovieService,
    private authService: AuthService,
    private movieCollectionService: MovieCollectionService
  ) { }

  // #Lifecycle hooks
  ngOnInit() {
    this.movieId = +this.route.snapshot.paramMap.get('movieId');

    this.movieDetailsService.getMovie(this.movieId).subscribe((movie: IMovieResult) => {
      this.truncatedOverview = movie.overview.substring(0, 50).concat('...');
      this.movie = movie;

      this.subscriptions.push(
        this.authService.isLoggedIn.subscribe(result => {
          this.isLoggedIn = result;
          if (this.isLoggedIn) {
            this.subscriptions.push(
              this.authService.getUserInfo().subscribe(user => {
                this.user = user;
                this.userHasMovie = this.movieCollectionService.userHasMovie(user, this.movie);
              })
            );
          }
        })
      );
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  getBackdropPath(path: string): string {
    return this.movieService.getBackdropPath(path);
  }

  getPosterPath(path: string): string {
    return this.movieService.getPosterPath(path);
  }

  getProductionCompanyLogoPath(path: string): string {
    return this.movieService.getProductionCompanyLogoPath(path);
  }

  updateMovie(isChecked: boolean) {
    if (isChecked) {
      this.saveMovie();
    } else {
      this.removeMovie();
    }
  }

  saveMovie() {
    this.subscriptions.push(
      this.movieCollectionService.saveMovieToProfile(this.user, this.movie).subscribe()
    );
    this.userHasMovie = true;
  }

  removeMovie() {
    this.subscriptions.push(
      this.movieCollectionService.removeMovieFromProfile(this.user, this.movie).subscribe()
    );
    this.userHasMovie = false;
  }
}
