import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { SharedModule } from './shared/shared.module';
import { SearchModule } from './search/search.module';
import { HttpClientModule } from '@angular/common/http';
import { DetailsModule } from './details/details.module';
import { SearchPersonModule } from './search-person/search-person.module';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { PersonDetailsModule } from './person-details/person-details.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PersonDetailsComponent
  ],
  imports: [
    BrowserModule,
    AuthModule,
    SharedModule,
    SearchModule,
    SearchPersonModule,
    HttpClientModule,
    DetailsModule,
    PersonDetailsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    BrowserAnimationsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
