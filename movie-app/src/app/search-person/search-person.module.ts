import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { SearchPersonComponent } from './search-person/search-person.component';
import { SearchPersonFiltersComponent } from './search-person-filters/search-person-filters.component';
import { SearchPersonResultsComponent } from './search-person-results/search-person-results.component';

const routes: Routes = [
  { path: 'search/person', component: SearchPersonComponent }
];

@NgModule({
  declarations: [SearchPersonComponent, SearchPersonFiltersComponent, SearchPersonResultsComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class SearchPersonModule { }
