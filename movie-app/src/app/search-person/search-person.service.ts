import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { IPerson } from '../shared/model';

@Injectable({
  providedIn: 'root'
})
export class SearchPersonService {
  readonly searchUrl = 'https://api.themoviedb.org/3/search/person?';

  private _people = new BehaviorSubject(null);
  people = this._people.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  setPeople(person: IPerson) {
    this._people.next(person);
  }

  searchPeople(query): Observable<IPerson> {
    return this.http.get<any>(`${this.searchUrl}query=${query}`)
    .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return throwError(err.message);
  }
}
