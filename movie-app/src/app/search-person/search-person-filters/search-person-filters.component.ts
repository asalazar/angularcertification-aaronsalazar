import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SearchPersonService } from '../search-person.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-person-filters',
  templateUrl: './search-person-filters.component.html',
  styleUrls: ['./search-person-filters.component.css']
})
export class SearchPersonFiltersComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private searchPersonService: SearchPersonService,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [null, Validators.required]
    });

    this.subscriptions.push(
      this.activatedRoute.queryParams.subscribe(query => {
        this.form.get('name').setValue(query ? query['query'] : null);
        this.search();
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscriptions => {
      subscriptions.unsubscribe();
    });
  }

  search() {
    // #Blocked-scoped variables `let` and `const`
    const values = this.form.value;
    this.searchPersonService.searchPeople(values.name).subscribe(result => {
      this.searchPersonService.setPeople(result);
    });
    if (values.name) {
      this.location.go(this.activatedRoute.snapshot.routeConfig.path, `query=${values.name}`);
    }
  }
}
