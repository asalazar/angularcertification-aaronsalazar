import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPersonFiltersComponent } from './search-person-filters.component';

describe('SearchPersonFiltersComponent', () => {
  let component: SearchPersonFiltersComponent;
  let fixture: ComponentFixture<SearchPersonFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPersonFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPersonFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
