import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SearchPersonService } from '../search-person.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService, IUser } from 'src/app/auth/auth.service';
import { IPerson, IPersonDetails, IPersonResult } from 'src/app/shared/model';
import { PersonCollectionService } from 'src/app/shared/person-collection.service';
import { map, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/shared/user.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-search-person-results',
  templateUrl: './search-person-results.component.html',
  styleUrls: ['./search-person-results.component.css']
})
export class SearchPersonResultsComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  isLoggedIn = false;
  user: IUser;
  dataSource: MatTableDataSource<IPerson>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  hasResults = false;

  constructor(
    private searchPersonService: SearchPersonService,
    private authService: AuthService,
    private personCollectionService: PersonCollectionService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.authService.isLoggedIn.subscribe(isLoggedIn => {
        this.isLoggedIn = isLoggedIn;
      })
    );

    this.subscriptions.push(
      this.authService.getUserInfo().pipe(
        switchMap(userInfo =>
          this.userService.getUserData(userInfo.id).pipe(
            map(userData => [userInfo, userData])
          )
        )
      ).subscribe(([userInfo, userData]) => {
        this.user = userInfo as IUser;
      })
    );

    this.searchPersonService.people.subscribe(people => {
      if (people) {
        this.dataSource = new MatTableDataSource(people.results);
        this.dataSource.paginator = this.paginator;

        if (people.results.length === 0) {
          this.hasResults = false;
        } else {
          this.hasResults = true;
        }
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  isInCollection(personDetails: IPersonDetails) {
    return this.personCollectionService.userHasPerson(this.user, personDetails);
  }

  addRemove(personDetails: IPersonDetails) {
    if (this.isInCollection(personDetails)) {
      this.subscriptions.push(
        this.removeFromCollection(personDetails).subscribe()
      );
    } else {
      this.subscriptions.push(
        this.addToCollection(personDetails).subscribe()
      );
    }
  }

  addToCollection(personDetails: IPersonDetails): Observable<IPersonDetails[]> {
    return this.personCollectionService.savePersonToProfile(this.user, personDetails);
  }

  removeFromCollection(personDetails: IPersonDetails): Observable<IPersonDetails[]> {
    return this.personCollectionService.removePersonFromProfile(this.user, personDetails)
  }

  getDisplayedColumns() {
    if (this.isLoggedIn) {
      return ['actions', 'name', 'known_for_department', 'navigate'];
    } else {
      return ['name', 'known_for_department', 'navigate'];
    }
  }
}
