import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPersonResultsComponent } from './search-person-results.component';

describe('SearchPersonResultsComponent', () => {
  let component: SearchPersonResultsComponent;
  let fixture: ComponentFixture<SearchPersonResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPersonResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPersonResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
