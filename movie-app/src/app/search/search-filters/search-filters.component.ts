import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SearchService } from '../search.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-filters',
  templateUrl: './search-filters.component.html',
  styleUrls: ['./search-filters.component.css']
})
export class SearchFiltersComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private searchService: SearchService,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    // #Forms
    this.form = this.formBuilder.group({
      search: [null, Validators.required], // #Validation
      year: [null]
    });

    this.subscriptions.push(
      this.activatedRoute.queryParams.subscribe(query => {
        this.form.get('search').setValue(query ? query['query'] : null);
        this.form.get('year').setValue(query ? query['year'] : null);
        this.search();
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  search() {
    const values = this.form.value;
    this.searchService.searchMovies(values.search, values.year).subscribe(result => {
      this.searchService.setMovies(result);
    });
    if (values.search || values.year) {
      this.location.go(this.activatedRoute.snapshot.routeConfig.path, `query=${values.search}&year=${values.year}`);
    }
  }
}
