import { TestBed } from '@angular/core/testing';
import { SearchService } from './search.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MovieKeyInterceptor } from '../shared/movie-key.interceptor';
import { IMovie } from '../shared/model';

describe('SearchService', () => {
  let searchService: SearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [SearchService, { provide: HTTP_INTERCEPTORS, useClass: MovieKeyInterceptor, multi: true }],
    });

    searchService = TestBed.get(SearchService);
  });

  it('should return an Observable<IMovie>', () => {
    expect(searchService).toBeTruthy();

    searchService.searchMovies('matrix', 1999).subscribe((movies: IMovie) => {
      expect(movies.results.length).toBe(3);
      expect(movies.results[0].title).toEqual('The Matrix');
      expect(movies.results[1].title).toEqual('V-World Matrix');
      expect(movies.results[2].title).toEqual('Darkdrive');
    });
  });
});
