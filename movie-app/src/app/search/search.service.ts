import { Injectable } from '@angular/core';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IMovie } from '../shared/model';


@Injectable({
  providedIn: 'root'
})
export class SearchService {
  readonly searchUrl = 'https://api.themoviedb.org/3/search/movie?';

  private _movies = new BehaviorSubject(null);
  movies = this._movies.asObservable();

  constructor(
    private http: HttpClient
  ) { }

  setMovies(movie: IMovie) {
    this._movies.next(movie);
  }

  searchMovies(query, year): Observable<IMovie> {
    return this.http.get<any>(`${this.searchUrl}query=${query}&page=1&include_adult=false&year=${year}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return throwError(err.message);
  }
}
