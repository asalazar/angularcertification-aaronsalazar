import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { SearchComponent } from './search/search.component';
import { SearchFiltersComponent } from './search-filters/search-filters.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'search/movie', component: SearchComponent },
];

@NgModule({
  declarations: [SearchComponent, SearchFiltersComponent, SearchResultsComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})

// #Modules
export class SearchModule { }
