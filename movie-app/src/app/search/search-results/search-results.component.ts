import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SearchService } from '../search.service';
import { IMovie, IMovieResult } from 'src/app/shared/model';
import { AuthService, IUser } from 'src/app/auth/auth.service';
import { MovieCollectionService } from 'src/app/shared/movie-collection.service';
import { switchMap, map } from 'rxjs/operators';
import { UserService } from 'src/app/shared/user.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  isLoggedIn = false;
  user: IUser;
  dataSource: MatTableDataSource<IMovie>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  hasResults = true;


  constructor(
    private searchService: SearchService,
    private authService: AuthService,
    private movieCollectionService: MovieCollectionService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.subscriptions.push(
      this.authService.isLoggedIn.subscribe(isLoggedIn => {
        this.isLoggedIn = isLoggedIn;
      })
    );

    this.subscriptions.push(
      this.authService.getUserInfo().pipe(
        switchMap(userInfo =>
          this.userService.getUserData(userInfo.id).pipe(
            map(userData => [userInfo, userData])
          )
        )
      ).subscribe(([userInfo, userData]) => {
        this.user = userInfo as IUser;
      })
    );

    this.subscriptions.push(
      this.searchService.movies.subscribe(movies => {
        if (movies) {
          this.dataSource = new MatTableDataSource(movies.results);
          this.dataSource.paginator = this.paginator;

          if (movies.results.length === 0) {
            this.hasResults = false;
          } else {
            this.hasResults = true;
          }
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  isInCollection(movie: IMovieResult) {
    return this.movieCollectionService.userHasMovie(this.user, movie);
  }

  addRemove(movieObj: IMovieResult) {
    if (this.isInCollection(movieObj)) {
      this.subscriptions.push(
        this.removeFromCollection(movieObj).subscribe()
      );
    } else {
      this.subscriptions.push(
        this.addToCollection(movieObj).subscribe()
      );
    }
  }

  addToCollection(movieResult: IMovieResult): Observable<IMovieResult[]> {
    return this.movieCollectionService.saveMovieToProfile(this.user, movieResult);
  }

  removeFromCollection(movieResult: IMovieResult) {
    return this.movieCollectionService.removeMovieFromProfile(this.user, movieResult);
  }

  getDisplayedColumns() {
    if (this.isLoggedIn) {
      return ['actions', 'name', 'release_date', 'overview', 'navigate'];
    } else {
      return ['name', 'release_date', 'overview', 'navigate'];
    }
  }
}
