import { Injectable } from '@angular/core';
import { of, Observable, BehaviorSubject } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  private readonly user: IUser = {
    id: 6,
    firstName: 'Aaron',
    lastName: 'Salazar'
  };

  private _isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
  isLoggedIn: Observable<boolean> = this._isLoggedIn.asObservable();

  redirectUrl: string;

  constructor(
    private router: Router
  ) { }

  login() {
    this._isLoggedIn.next(true);
  }

  getUserInfo(): Observable<IUser> {
    if (this._isLoggedIn) {
      return of(this.user);
    } else {
      return of(null);
    }
  }

  logout() {
    this._isLoggedIn.next(false);
    this.router.navigate(['/login']);
  }
}

// #interface
export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
}
