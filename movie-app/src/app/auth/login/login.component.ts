import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  form: FormGroup;
  hide = true;
  isLoggingIn = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: [null, Validators.required], // #Validation
      password: [null, Validators.required] // #Validation
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  login() {
    this.isLoggingIn = true;
    this.authService.login();

    this.subscriptions.push(
      this.authService.isLoggedIn.subscribe(isLoggedIn => {
        this.isLoggingIn = false;
        if (isLoggedIn) {
          this.router.navigate(['/dashboard']);
        }
      })
    );
  }
}
