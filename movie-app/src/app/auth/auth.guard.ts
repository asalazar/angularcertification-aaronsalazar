import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
// #Guards
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
  Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // #Blocked-scoped variables `let` and `const`
    const url: string = state.url;
    return this.checkLogin(url);
  }

  // #Map
  private checkLogin(url: string): Observable<boolean> {
    return this.authService.isLoggedIn.pipe(
      map(result => {
        if (result) { return true; }

        this.authService.redirectUrl = url;
        this.router.navigate(['/login']);
        return false;
      })
    );
  }
}
